/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#pragma once

enum Direction { NORTH, SOUTH, EAST, WEST };

enum RotationDirection { CLOCKWISE, COUNTERCLOCKWISE };

extern Direction opposite_direction(Direction dir);
