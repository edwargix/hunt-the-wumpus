/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include "helpers.hpp"

#include <SFML/Graphics.hpp>
#include <set>
#include <stdlib.h>

#include "Direction.hpp"
#include "MapNode.hpp"

// totally arbitrary map
MapNode *create_map_nodes()
{
  MapNode *center = new MapNode();

  MapNode *n, *e, *e2, *w1, *w2;

  n = center->insert(NORTH);
  e = n->insert(EAST);
  e = e->insert(EAST);
  e = e->insert(EAST);
  e = e->insert(NORTH);
  e = e->insert(NORTH);
  n = n->insert(NORTH);
  n = n->insert(NORTH);
  e2 = n->insert(EAST);
  e2 = e2->insert(EAST);
  e2->east = e; e->west = e2;
  n = n->insert(NORTH);
  n = n->insert(NORTH);
  n = n->insert(WEST);
  n = n->insert(WEST);
  n = n->insert(WEST);
  n = n->insert(SOUTH);
  n = n->insert(SOUTH);
  w1 = n->insert(SOUTH);
  n = w1->insert(WEST);
  w2 = n->insert(SOUTH);
  n = w2->insert(SOUTH);
  n = n->insert(SOUTH);
  n = n->insert(SOUTH);
  n = n->insert(EAST);
  n = n->insert(EAST);
  n = n->insert(EAST);
  e = n->insert(EAST);
  e = n->insert(EAST);
  n = n->insert(NORTH);

  n = n->insert(NORTH);
  n->east = center; center->west = n;
  n = n->insert(NORTH);
  n->east = center->north; center->north->west = n;
  e = n->insert(NORTH);
  e->east = center->north->north; center->north->north->west = e;

  n = n->insert(WEST);
  e = e->insert(WEST);
  n->north = e; e->south = n;

  e->west = w1; w1->east = e;
  n = n->insert(WEST);
  n->west = w2; w2->east = n;
  n->north = w1; w1->south = n;


  return center;
}


static std::set<MapNode *> delete_check;
void __delete_map_nodes(MapNode *node)
{
  if (node->north)
    node->north->south = NULL;
  if (node->south)
    node->south->north = NULL;
  if (node->east)
    node->east->west = NULL;
  if (node->west)
    node->west->east = NULL;

  delete_check.insert(node);

  if (node->north && delete_check.find(node->north) == delete_check.end())
    __delete_map_nodes(node->north);
  if (node->south && delete_check.find(node->south) == delete_check.end())
    __delete_map_nodes(node->south);
  if (node->east && delete_check.find(node->east) == delete_check.end())
    __delete_map_nodes(node->east);
  if (node->west && delete_check.find(node->west) == delete_check.end())
    __delete_map_nodes(node->west);

  free(node);
}

void delete_map_nodes(MapNode *node)
{
  delete_check.clear();
  __delete_map_nodes(node);
}


static std::set<const MapNode *> count_check;
void __count_map_nodes(const MapNode *node, size_t& count)
{
  ++count;

  count_check.insert(node);

  if (node->north && count_check.find(node->north) == count_check.end())
    __count_map_nodes(node->north, count);
  if (node->south && count_check.find(node->south) == count_check.end())
    __count_map_nodes(node->south, count);
  if (node->east && count_check.find(node->east) == count_check.end())
    __count_map_nodes(node->east, count);
  if (node->west && count_check.find(node->west) == count_check.end())
    __count_map_nodes(node->west, count);
}


size_t count_map_nodes(const MapNode *node)
{
  size_t count = 0;
  count_check.clear();
  __count_map_nodes(node, count);
  return count;
}


static std::set<const MapNode *> checked;
const MapNode *__random_node(const MapNode *node, size_t& count)
{
  if (rand() % count == 0) // has a 1 in `count` chance of happening
    return node;

  checked.insert(node);

  const MapNode *ret;
  if (node->north && checked.find(node->north) == checked.end()) {
    if ((ret = __random_node(node->north, count)) != NULL)
      return ret;
  }
  if (node->south && checked.find(node->south) == checked.end()) {
    if ((ret = __random_node(node->south, count)) != NULL)
      return ret;
  }
  if (node->east && checked.find(node->east) == checked.end()) {
    if ((ret = __random_node(node->east, count)) != NULL)
      return ret;
  }
  if (node->west && checked.find(node->west) == checked.end()) {
    if ((ret = __random_node(node->west, count)) != NULL)
      return ret;
  }

  return NULL;
}


const MapNode *random_node(const MapNode *node)
{
  const MapNode *ret;
  srand(time(NULL));
  size_t count = count_map_nodes(node);
  do {
    checked.clear();
    ret = __random_node(node, count);
  } while (ret == NULL);
  return ret;
}


bool move_node_ptr(const MapNode **node, Direction dir)
{
  switch (dir) {
  case NORTH:
    if ((*node)->north) *node = (*node)->north;
    else return false;
    return true;
  case SOUTH:
    if ((*node)->south) *node = (*node)->south;
    else return false;
    return true;
  case EAST:
    if ((*node)->east) *node = (*node)->east;
    else return false;
    return true;
  case WEST:
    if ((*node)->west) *node = (*node)->west;
    else return false;
    return true;
  }

  // impossible, remove warning
  return false;
}


void random_move(const MapNode **node)
{
  bool north = false, south = false, east = false, west = false;
  short count = 0;
  if ((*node)->north) {
    north = true;
    ++count;
  }
  if ((*node)->south) {
    south = true;
    ++count;
  }
  if ((*node)->east) {
    east = true;
    ++count;
  }
  if ((*node)->west) {
    west = true;
    ++count;
  }

  srand(time(NULL));
  int r = rand() % count;

  if (north && --r < 0) {
    (*node) = (*node)->north;
    return;
  }
  if (south && --r < 0) {
    (*node) = (*node)->south;
    return;
  }
  if (east && --r < 0) {
    (*node) = (*node)->east;
    return;
  }
  if (west && --r < 0) {
    (*node) = (*node)->west;
    return;
  }
}


bool find_node_in_star(const MapNode *node, const MapNode *center_of_star)
{

  if (center_of_star == node) return true;

  if (center_of_star->north) {
    if (center_of_star->north == node) return true;
    if (center_of_star->north->north == node) return true;
    if (center_of_star->north->south == node) return true;
    if (center_of_star->north->east == node) return true;
    if (center_of_star->north->west == node) return true;
  }

  if (center_of_star->south) {
    if (center_of_star->south == node) return true;
    if (center_of_star->south->north == node) return true;
    if (center_of_star->south->south == node) return true;
    if (center_of_star->south->east == node) return true;
    if (center_of_star->south->west == node) return true;
  }

  if (center_of_star->east) {
    if (center_of_star->east == node) return true;
    if (center_of_star->east->north == node) return true;
    if (center_of_star->east->south == node) return true;
    if (center_of_star->east->east == node) return true;
    if (center_of_star->east->west == node) return true;
  }

  if (center_of_star->west) {
    if (center_of_star->west == node) return true;
    if (center_of_star->west->north == node) return true;
    if (center_of_star->west->south == node) return true;
    if (center_of_star->west->east == node) return true;
    if (center_of_star->west->west == node) return true;
  }

  return false;
}


bool node_in_trajectory(const MapNode *start,
                        const Direction& direction,
                        const MapNode *looking_for)
{
  if (start == looking_for) return true;

  switch (direction) {
  case NORTH:
    if (start->north)
      return node_in_trajectory(start->north, direction, looking_for);
    return false;
  case SOUTH:
    if (start->south)
      return node_in_trajectory(start->south, direction, looking_for);
    return false;
  case EAST:
    if (start->east)
      return node_in_trajectory(start->east, direction, looking_for);
    return false;
  case WEST:
    if (start->west)
      return node_in_trajectory(start->west, direction, looking_for);
    return false;
  }
}
