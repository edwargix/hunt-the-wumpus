Names: Zack Hart and David Florness
Seciton: A

# Hunt the Wumpus
This is a remake of an early computer game called "Hunt the Wumpus".

This game could originally be played in 1975 on computers such as the
TI-99/4A (1980).

# How to play:
Rotate the character with the left and right arrow keys and then press
the forward/back keys to move.  Press space bar to shoot one of your
arrows to attempt to kill the Wumpus. You only have 5 arrows, however,
so use them wisely.

The objective of the game is to uncover the tiles in the room to find
out where the Wumpus is and use one of your arrows to kill him.  While
navigating the room, you have to avoid stepping on the same tile as
the Wumpus. Bloody tiles indicate that you are within 2 tiles of the
wumpus.

The Wumpus can move freely whenever you move your character, and you
need to move over a tile to update it (Ex: if a tile is bloody one
turn and the wumpus moves out of its range, you must move over it
again to see if it's still bloody. If the wumpus is no longer in
range, it will no longer be bloody.)  In order to win, you must
correctly assume which tile the Wumpus is on during that turn. You
must rotate your character in the direction of the adjacent tile you
think it is on, and then press spacebar to shoot.  Rotating does not
consuming a turn, however moving forward/back does.  There is no
penalty for missing your shot, except that you have one less arrow in
your quiver, and when your quiver runs out, you lose the game.  No
matter how you play it, because of the Wumpus's random movement, there
will always be guess work involved, and you may not always win.

# Problem Description
We wanted to recreate a retro game called “Hunt the Wumpus.” Hunt the
Wumpus consists of a randomized map which has a few different elements
to challenge the user in reaching the ultimate goal: to kill the
monster called the Wumpus. The user navigates the map avoiding slime
pits, which are indicated by “slimy rooms” within two tiles, and would
kill the user if walked into, bats, which randomly transport the user
to a different room, and the Wumpus itself, which is indicated by
bloody rooms within two tiles. The user must aim and shoot at the
Wumpus with a limited (usually 3-5) number of arrows to win the game
(note that if the user walks into the room with the Wumpus, they
die). The map is initially all hidden, but tiles are uncovered as the
user walks through them.

# Program Documentation
## Compiling and running
All one has to do to compile this program is to run the following
command:

	$ make

This will compile using the approriate standard of C++, skip source
files that have already been compiled, and include and link the
necessary SFML library. The executable is named `hunt-the-wumpus`, so
one can run the program with the following command:

	$ ./hunt-the-wumpus

## For hackers: extending the program
For the readers who wish to improve the program, we have done our best
to modularize the main componenets. Althought main.cpp is quite large,
it is compartmentalized into what we like to call procedures. The
names should be self-explanatory. The easiest thing an aspiring
programmer can change is the map, which is hard-coded into the
function `create_map_modes` in the file helpers.cpp. It simply uses
the MapNode features to create a network of nodes (which each
represent a tile on the map). A feature that we wish we had time to
implement is the ability to import custom-made maps into the game from
files.

# Tests
User can rotate and move the player using the arrow keys User can
shoot and kill the Wumpus, and it results in a victory The Wumpus is
indicated by bloody tiles within 2 tiles away The user can navigate
all rooms and cannot navigate outside of the map area Running into the
wumpus kills the player and results in a loss Running out of arrows
results in a loss Rotating the player does not use a turn, but moving
forward/backwards does Sprites and animations load correctly, and the
arrow animation successfully goes across the screen Whenever the
Wumpus is no longer in range of a previously bloody tile, and the
player walks over that tile, it updates so that it is no longer bloody
Wumpus spawns in a random location and moves randomly between turns
Wumpus is invisible until either killing the player or being killed by
the player.

# Reflection
The most important thing we've taken away form this project is that
having well-defined, intuituve, and rational data structures makes the
code exponentially easier. The MapNode struct is easy to extend upon,
and functions which utilize it are easy to fully grasp even for an
outsider. The one thing we would do differently is start a little
earlier to be able to finish those afore-mentioned missing features.
