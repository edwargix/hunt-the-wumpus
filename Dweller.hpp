/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#pragma once

#include <SFML/Graphics.hpp>

#include "MapNode.hpp"
#include "Direction.hpp"


class Dweller : public sf::Sprite {
public:
  Dweller(const MapNode *map_position = NULL,
          Direction direction = NORTH,
          bool visible = false);

  const MapNode *map_position;
  Direction direction;

  void rotate(RotationDirection);
  bool getVisible(void);
  void setVisible(bool visible);

private:
  bool visible;
};
