/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include "Bullet.hpp"


#define LONG 25.0f
#define SHORT 10.0f


Bullet::Bullet() : in_motion(false)
{
}


void Bullet::set_direction(Direction dir)
{
  direction = dir;
  switch (dir) {
  case NORTH:
  case SOUTH:
    this->setSize(sf::Vector2f(SHORT, LONG));
    this->setOrigin(sf::Vector2f(SHORT / 2, LONG / 2));
    return;
  case EAST:
  case WEST:
    this->setSize(sf::Vector2f(LONG, SHORT));
    this->setOrigin(sf::Vector2f(LONG / 2, SHORT / 2));
    return;
  }
}


Direction Bullet::get_direction()
{
  return direction;
}


void Bullet::move(float f)
{
  switch (direction) {
  case NORTH:
    sf::RectangleShape::move(0, -f);
    return;
  case SOUTH:
    sf::RectangleShape::move(0, f);
    return;
  case EAST:
    sf::RectangleShape::move(f, 0);
    return;
  case WEST:
    sf::RectangleShape::move(-f, 0);
    return;
  }
}
