/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <map>
#include "MapNode.hpp"
#include "Direction.hpp"

class Map {
public:
  Map(const MapNode *center);

  void draw(sf::RenderWindow& window);
  void move(const sf::Vector2f& offset);
  void setPosition(const sf::Vector2f& pos);

  void center_adjust(const Direction& direction);

  void move_sprite_to_node(sf::Sprite& sprite, const MapNode *node);

  void make_node_bloody(const MapNode *node);
  void make_node_unbloody(const MapNode *node);

private:
  const MapNode *center;

  std::map<const MapNode *, sf::Sprite> node_sprites;
  std::map<const MapNode *, bool> is_node_bloody;
  void move_sprites(const MapNode *center, sf::Vector2f);
  void __move_sprites(const MapNode *, sf::Vector2f);

  static sf::Texture node_texture;
  static sf::Texture bloody_node_texture;
};
