/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include <SFML/Graphics.hpp>

#include "Direction.hpp"


class Bullet : public sf::RectangleShape
{
private:
  Direction direction;

public:
  Bullet();

  void set_direction(Direction dir);

  Direction get_direction();

  void move(float f);

  bool in_motion;
};
