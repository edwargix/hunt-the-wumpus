/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include <stdlib.h>

#include "MapNode.hpp"
#include "Direction.hpp"


MapNode::MapNode()
  : north(NULL), south(NULL), east(NULL), west(NULL)
{
}


MapNode *MapNode::insert(Direction direction)
{
  switch (direction) {
  case NORTH: {
    if (north) return north;

    MapNode *new_node = new MapNode();
    this->north = new_node;
    new_node->south = this;
    return new_node;
  }
  case SOUTH: {
    if (south) return south;

    MapNode *new_node = new MapNode();
    this->south = new_node;
    new_node->north = this;
    return new_node;
  }
  case EAST: {
    if (east) return east;

    MapNode *new_node = new MapNode();
    this->east = new_node;
    new_node->west = this;
    return new_node;
  }
  case WEST: {
    if (west) return west;

    MapNode *new_node = new MapNode();
    this->west = new_node;
    new_node->east = this;
    return new_node;
  }
  }

  // impossible, but disables warning
  return NULL;
}
