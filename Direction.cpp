/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include "Direction.hpp"


extern Direction opposite_direction(Direction dir)
{
  switch (dir) {
  case NORTH:
    return SOUTH;
  case SOUTH:
    return NORTH;
  case EAST:
    return WEST;
  case WEST:
    return EAST;
  }

  // impossible, removes warning
  return NORTH;
}
