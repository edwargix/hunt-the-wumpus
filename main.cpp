/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <sstream>
#include <set>

#include "helpers.hpp"
#include "Map.hpp"
#include "MapNode.hpp"
#include "Dweller.hpp"
#include "Bullet.hpp"

using namespace sf;
using std::set;


#define TITLE "Hunt the Wumpus"
#define DEFAULT_DIMENSIONS 640, 480
#define BULLET_SPEED 1
#define NUM_SHOTS 5
#define FONT_FILE "./fonts/DejaVuSans.ttf"


// global procedure variables
static RenderWindow window(VideoMode(DEFAULT_DIMENSIONS), TITLE);
static Map *map;
static Dweller player;
static Dweller wumpus;
static Bullet bullet;
static int num_shots_left = NUM_SHOTS;
static Vector2f middle_of_window;
static set<void (*)()> hooks; // hooks are functions that get run
                              // repeatedly in the main game loop
static bool won = false;
static bool lost = false;


// procedures
void setup_map();
void setup_sprites();
void setup_text();
void get_input();
void update_objects();
void draw_objects();
void move(Direction d);
void shoot(Direction d);
void game_done();
void cleanup();


int main(void)
{
  // Hooks: functions that will execute during the game loop
  hooks.insert(get_input);
  hooks.insert(update_objects);
  hooks.insert(draw_objects);

  setup_map();
  setup_sprites();
  setup_text();

  // Main game loop
  while (window.isOpen()) {
    middle_of_window.x = window.getSize().x / 2;
    middle_of_window.y = window.getSize().y / 2;

    window.clear();

    // use a copy incase a hook adds another hook
    set<void (*)()> hooks_cpy = hooks;
    for (auto it = hooks_cpy.begin(); it != hooks_cpy.end(); ++it)
      (*it)();

    // Exit program when window is closed
    Event event;
    while (window.pollEvent(event)) {
      if (event.type == Event::Closed)
        window.close();
    }
    window.display();
  }

  cleanup();

  return 0;
}


static MapNode *map_nodes;
void setup_map()
{
  map_nodes = create_map_nodes();

  map = new Map(map_nodes);
}


void setup_sprites()
{
  static Texture player_texture = Texture();
  if (!player_texture.loadFromFile("sprites/player.png"))
    exit(1);

  static Texture wumpus_texture = Texture();
  if (!wumpus_texture.loadFromFile("sprites/wumpus.png"))
    exit(1);

  player.setTexture(player_texture);
  player.setOrigin(player_texture.getSize().x / 2,
                   player_texture.getSize().y / 2);
  player.map_position = map_nodes;
  player.setVisible(true);

  wumpus.setTexture(wumpus_texture);
  wumpus.setOrigin(wumpus_texture.getSize().x / 2,
                   wumpus_texture.getSize().y / 2);
  // ensure the wumpus does not start right next to or on top of the player
  do {
    wumpus.map_position = const_cast<MapNode *>(random_node(map_nodes));
  } while (find_node_in_star(wumpus.map_position, player.map_position));

  wumpus.setVisible(false);
}


// text varibles
static std::ostringstream shots_string;
static Text shots_remaining_text;
static Text game_over_text;
static Text game_won_text;
void setup_text()
{
  static Font font = Font();
  if (!font.loadFromFile(FONT_FILE))
    exit(1);

  shots_remaining_text.setFont(font);
  shots_remaining_text.setCharacterSize(24);
  shots_remaining_text.setColor(Color::White);

  game_over_text.setString("Game Over!");
  game_over_text.setFont(font);
  game_over_text.setColor(Color::Red);

  game_won_text.setString("You won!");
  game_won_text.setFont(font);
  game_won_text.setColor(Color::Green);
}


void get_input()
{
  static bool last_press;
  if (Keyboard::isKeyPressed(Keyboard::Left)) {
    if (!last_press)
      player.rotate(COUNTERCLOCKWISE);
    last_press = true;
  }
  else if (Keyboard::isKeyPressed(Keyboard::Right)) {
    if (!last_press)
      player.rotate(CLOCKWISE);
    last_press = true;
  }
  else if (Keyboard::isKeyPressed(Keyboard::Up)) {
    if (!last_press)
      move(player.direction);
    last_press = true;
  }
  else if (Keyboard::isKeyPressed(Keyboard::Down)) {
    if (!last_press)
      move(opposite_direction(player.direction));
    last_press = true;
  }
  else if (Keyboard::isKeyPressed(Keyboard::Space)) {
    if (!last_press)
      shoot(player.direction);
  }
  else
    last_press = false;
}


void update_objects()
{
  player.setPosition(middle_of_window);

  map->setPosition(middle_of_window);

  map->move_sprite_to_node(wumpus, wumpus.map_position);
}


void draw_objects()
{
  map->draw(window);

  // checks whether bullet is in window
  if (Rect<unsigned>(Vector2u(0, 0), window.getSize()).
      contains((Vector2u)bullet.getPosition())) {
    window.draw(bullet);
    bullet.move(BULLET_SPEED);
  } else {
    bullet.in_motion = false;
  }

  if (player.getVisible())
    window.draw(player);
  if (wumpus.getVisible())
    window.draw(wumpus);

  shots_string << "Shots remaining: " << num_shots_left;
  shots_remaining_text.setString(shots_string.str());
  shots_string.str(std::string()); // empty string
  shots_remaining_text.setPosition(Vector2f(0, 0));

  window.draw(shots_remaining_text);

  if (lost) {
    game_over_text.setPosition(middle_of_window);
    window.draw(game_over_text);
  }
  else if (won) {
    game_won_text.setPosition(middle_of_window);
    window.draw(game_won_text);
  }
}


void move(Direction d)
{
  if (move_node_ptr(&player.map_position, d)) {
    map->center_adjust(d);
    random_move(&wumpus.map_position);

    if (player.map_position == wumpus.map_position) {
      game_done();
      lost = true;
    }

    if (find_node_in_star(player.map_position, wumpus.map_position))
      map->make_node_bloody(player.map_position);
    else
      map->make_node_unbloody(player.map_position);
  }
}


void shoot(Direction d)
{
  if (!bullet.in_motion) {
    bullet.set_direction(d);
    bullet.setPosition(middle_of_window);
    bullet.in_motion = true;
    if (node_in_trajectory(player.map_position,
                           player.direction,
                           wumpus.map_position)) {
      game_done();
      won = true;
    }
    else if (--num_shots_left < 1) {
      game_done();
      lost = true;
    }
  }
}


void game_done()
{
  wumpus.setVisible(true);
  hooks.erase(get_input);
}


void cleanup()
{
  delete_map_nodes(map_nodes);
  delete map;
}
