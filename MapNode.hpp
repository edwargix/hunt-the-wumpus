/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#pragma once

#include "Direction.hpp"

struct MapNode {
  MapNode();

  MapNode *north;
  MapNode *south;
  MapNode *east;
  MapNode *west;

  MapNode *insert(Direction direction);
};
