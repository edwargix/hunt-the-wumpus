/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include "Map.hpp"
#include <iostream>
#include <set>

sf::Texture Map::node_texture = sf::Texture();
sf::Texture Map::bloody_node_texture = sf::Texture();

bool set = false;
size_t i;

Map::Map(const MapNode *center)
{
  if (!set) {
    Map::node_texture.loadFromFile("sprites/roombase.png");
    Map::bloody_node_texture.loadFromFile("sprites/roomblood.png");
  }
  this->center = center;
  move_sprites(this->center, sf::Vector2f(0, 0));
}


void Map::draw(sf::RenderWindow& window)
{
  for (auto it = node_sprites.begin(); it != node_sprites.end(); ++it)
    window.draw(it->second);
}


void Map::move(const sf::Vector2f& offset)
{
  for (auto it=node_sprites.begin(); it != node_sprites.end(); ++it)
    it->second.move(offset);
}


void Map::setPosition(const sf::Vector2f& pos)
{
  move_sprites(center, pos);
}


void Map::center_adjust(const Direction& direction)
{
  sf::Vector2f curr_pos = node_sprites[center].getPosition();

  switch (direction) {
  case NORTH:
    if (center->north)
      center = center->north;
    break;
  case SOUTH:
    if (center->south)
      center = center->south;
    break;
  case EAST:
    if (center->east)
      center = center->east;
    break;
  case WEST:
    if (center->west)
      center = center->west;
    break;
  }

  setPosition(curr_pos);
}


void Map::move_sprite_to_node(sf::Sprite& sprite, const MapNode *node)
{
  sprite.setPosition(node_sprites[node].getPosition());
}


void Map::make_node_bloody(const MapNode *node)
{
  if (is_node_bloody.find(node) != is_node_bloody.end() &&
      is_node_bloody[node] == false) {
    node_sprites[node].setTexture(bloody_node_texture);
    is_node_bloody[node] = true;
  }
}


void Map::make_node_unbloody(const MapNode *node)
{
  if (is_node_bloody.find(node) != is_node_bloody.end() &&
      is_node_bloody[node] == true) {
    node_sprites[node].setTexture(node_texture);
    is_node_bloody[node] = false;
  }
}


static std::set<const MapNode *> moved;
void Map::__move_sprites(const MapNode *cur, sf::Vector2f offset)
{
  if (node_sprites.find(cur) == node_sprites.end()) {
    static sf::Sprite new_sprite = sf::Sprite();
    new_sprite.setTexture(node_texture);
    new_sprite.setOrigin(node_texture.getSize().x / 2,
                         node_texture.getSize().y / 2);
    new_sprite.setPosition(offset);
    node_sprites[cur] = new_sprite;
    is_node_bloody[cur] = false;
  } else {
    node_sprites[cur].setPosition(offset);
  }

  moved.insert(cur);

  if (cur->north && moved.find(cur->north) == moved.end()) {
    offset.y -= node_texture.getSize().y;
    __move_sprites(cur->north, offset);
    offset.y += node_texture.getSize().y;
  }
  if (cur->south && moved.find(cur->south) == moved.end()) {
    offset.y += node_texture.getSize().y;
    __move_sprites(cur->south, offset);
    offset.y -= node_texture.getSize().y;
  }
  if (cur->east && moved.find(cur->east) == moved.end()) {
    offset.x += node_texture.getSize().x;
    __move_sprites(cur->east, offset);
    offset.x -= node_texture.getSize().x;
  }
  if (cur->west && moved.find(cur->west) == moved.end()) {
    offset.x -= node_texture.getSize().x;
    __move_sprites(cur->west, offset);
    offset.x += node_texture.getSize().x;
  }
}


void Map::move_sprites(const MapNode *center, sf::Vector2f offset)
{
  moved.clear();
  __move_sprites(center, offset);
}
