SOURCES = $(wildcard *.cpp)
HEADERS = $(wildcard *.hpp)
OBJECTS = $(SOURCES:.cpp=.o)

# name of the executable
TARGET = hunt-the-wumpus

CXX    = g++
CFLAGS = -Wall -O3 -std=c++11

INCPATH += -I/usr/include
LIBPATH += -L/usr/lib
LIBS += -lsfml-graphics -lsfml-window -lsfml-system

# If the first argument is "debug"...
ifeq (debug,$(firstword $(MAKECMDGOALS)))
  CFLAGS += -g -ggdb3 -o0
endif

all: $(TARGET)

$(TARGET): $(OBJECTS)
	$(CXX) $(CFLAGS) -o $@ $^ $(LIBPATH) $(LIBS)

debug: $(TARGET)

clean:
	rm -f .depend $(OBJECTS) $(TARGET)

%.o: %.cpp
	$(CXX) $(CFLAGS) $(INCPATH) -c -o $@ $<

depend: .depend

.depend: $(SOURCES)
	rm -f .depend
	$(CXX) $(CFLAGS) -MM $^ >> .depend;

include .depend

compile: $(OBJECTS)

run: $(TARGET)
	./$(TARGET)

tar: $(SOURCES) $(HEADERS) Makefile fonts/* proposal/* sprites/*
	tar czvf hunt-the-wumpus.tar.gz $^
