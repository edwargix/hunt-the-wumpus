/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#pragma once

#include <SFML/Graphics.hpp>

#include "MapNode.hpp"


MapNode *create_map_nodes();

void delete_map_nodes(MapNode *node);

size_t count_map_nodes(const MapNode *node);

const MapNode *random_node(const MapNode *node);

bool move_node_ptr(const MapNode **node, Direction dir);

void random_move(const MapNode **node);

bool find_node_in_star(const MapNode *node, const MapNode *center_of_star);

bool node_in_trajectory(const MapNode *start,
                        const Direction& direction,
                        const MapNode *looking_for);
