/* CSCI 261: Final Project: Hunt the Wumpus
 *
 * Author: Zack Hart and David Florness
 * Section: A
 *
 * A remake of an early computer game called "Hunt the Wumpus".
 */

#include "Dweller.hpp"


Dweller::Dweller(const MapNode *map_position, Direction direction, bool visible)
  : map_position(map_position), direction(direction), visible(visible)
{
}


float directionToAngle(Direction rdir)
{
  switch (rdir) {
  case NORTH:
    return 0.0f;
  case SOUTH:
    return 180.0f;
  case EAST:
    return 90.0f;
  case WEST:
    return 270.0f;
  }

  // impossible, removes warning
  return 0.0f;
}


void Dweller::rotate(RotationDirection rdir)
{
  switch (this->direction) {
  case NORTH:
    direction = rdir == CLOCKWISE ? EAST : WEST;
    break;
  case SOUTH:
    direction = rdir == CLOCKWISE ? WEST : EAST;
    break;
  case EAST:
    direction = rdir == CLOCKWISE ? SOUTH : NORTH;
    break;
  case WEST:
    direction = rdir == CLOCKWISE ? NORTH : SOUTH;
    break;
  }
  setRotation(directionToAngle(direction));
}


bool Dweller::getVisible() {
  return visible;
}


void Dweller::setVisible(bool visible) {
  this->visible = visible;
};
